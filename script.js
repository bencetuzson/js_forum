'use strict'

function SendPost() {
    console.log('SendPost');
    let thread = document.getElementById("Thread");
    let author = document.getElementById("Author");
    let content = document.getElementById("Content");

    let newPost = document.createElement("dt");

    newPost.innerText = `${author.value} : ${content.value}`;

    thread.appendChild(newPost);
}

function loadPage() {
    console.log("Page loaded.");
    let button = document.getElementById("SendButton");
    button.addEventListener('click', SendPost);
};

window.addEventListener('load', loadPage);